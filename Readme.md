# demorpackage

<!-- badges: start -->
[![Lifecycle: experimental](https://lifecycle.r-lib.org/articles/figures/lifecycle-experimental.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

"*Le package n’est pas le Saint Graal, mais plutôt la brique de base dans R*"

Pour créer un package R, suivre les recommandations proposées par [thinkr](https://thinkr.fr/creer-package-r-quelques-minutes/).
Documentation officielle : [https://r-pkgs.org/tests.html](https://r-pkgs.org/tests.html)

## Comment créer un paquet ?

```r
devtools::create("demorpackage")
```

`devtools` créé l'arborescence que vous pouvez compléter avec `inst/extdata` et un fichier `Readme.md` de manière à avoir :


```
.
├── DESCRIPTION # Description du package, à remplir à la main
├── devtools_history.R # Bonne pratique. Historique des commandes lancées pour créer le package
├── inst
│   └── extdata # Répertoire pour stocker les fichiers externes (p.ex pour les tests)
│       └── FR_LGT_001_20_036_0000.csv 
├── man # Documentation générée automatiquement
├── NAMESPACE # Ne jamais modifier le contenu
├── R
│   └── readRawFilesBM.R # Les fonctions du package (1 fichier regroupant un type de fonction ou 1 fichier par fonction)
└── Readme.md # Descriptif du projet git (comment utiliser/lancer le package)
```

## Principales commandes

```r
devtools::load_all() # Chargement de toutes les fonctions pour tester
devtools::test() # Lancement des tests (testthat)
devtools::check()
devtools::build()

```

- Ajout des dépendances :

```r
usethis::use_package("nom_package")
```

## Installation

Vous pouvez installer le package demorpackage :

```r
devtools::install_gitlab("urep/docs/demorpackage",host="https://forgemia.inra.fr")
```

## Exemple d'utilisation

Ici, ajouter des exemples d'utilisation de la fonction

```r

```

